package main

import (
	"fmt"
	"sort"
)

// alias
type byLength []string

// We implement sort.Interface - Len, Less, Swap
// so we can use the generic function Sort
func (s byLength) Len() int {
	return len(s)
}

func (s byLength) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s byLength) Less(i, j int) bool {
	return len(s[i]) < len(s[j])
}

func main() {
	fruits := []string{"peach", "banana", "kikiw"}
	sort.Sort(byLength(fruits))
	fmt.Println(fruits)
}
