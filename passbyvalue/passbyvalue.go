// https://stackoverflow.com/questions/32700999/pointer-arithmetic-in-go
//
//    Why is there no pointer arithmetic?
//
// Safety. Without pointer arithmetic it's possible to create a language that can never derive an illegal
// address that succeeds incorrectly. Compiler and hardware technology have advanced to the point where a
// loop using array indices can be as efficient as a loop using pointer arithmetic. Also, the lack of pointer
// arithmetic can simplify the implementation of the garbage collector.

package main

import (
	"fmt"
)

func incr(a int) {
	a = a + 1
}

func main() {
	a := 10
	incr(a)
	fmt.Println(a)
}
