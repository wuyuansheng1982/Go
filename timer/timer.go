package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	// A function returning a channel
	c := boring("boring!")

	for i:=0; i<5; i++ {
		// Read a string from the channel and print it
		fmt.Printf("You say: %q\n", <-c)
	}

	fmt.Println("You're boring; I am leaving.")
}

// A generator function that returns a receive-only channel of strings.

func boring(msg string) <-chan string {
	// Create the channel of strings
	c := make(chan string)

	// Define an anonymous function and run it as a goroutine
	go func() {
		for i := 0; ;i++ {
			// Send a message to the channel
			c <- fmt.Sprintf("%s %d", msg, i)
			time.Sleep(time.Duration(rand.Intn(1e3)) * time.Millisecond)
		}
	}()
	return c
}