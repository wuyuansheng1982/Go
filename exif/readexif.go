package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"

	"github.com/rwcarlsen/goexif/exif"
)

func readjpegdata(dirname string) {
	files, err := ioutil.ReadDir(dirname)
	if err != nil {
		panic(err)
	}
	for _, file := range files {
		if (path.Ext(file.Name()) == ".jpg") || (path.Ext(file.Name()) == ".jpeg") {
			fpath := path.Join(dirname, file.Name())
			//println(fpath)
			f, err := os.Open(fpath)
			if err != nil {
				panic(err)
			}
			defer f.Close()
			x, err := exif.Decode(f)
			if err != nil {
				panic(err)
			}
			tm, _ := x.DateTime()

			lat, long, _ := x.LatLong()
			fmt.Println(file.Name(), " | ", tm, " | lat, long: ", lat, ", ", long)
		}
	}
}

func main() {
	readjpegdata("D:\\BaiduNetdiskDownload")
}
