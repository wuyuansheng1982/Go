package main

import (
	"fmt"
)

func whatAmI(i interface{}) {
	switch t := i.(type) {
	case bool:
		fmt.Println("boolean")
	case int:
		fmt.Println("int")
	default:
		fmt.Println("don't know %T", t)
	}
}

func main() {
	whatAmI(true)
	whatAmI(2)
}
