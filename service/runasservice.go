package main

import (
	"log"
	"os"
	"os/exec"

	"github.com/kardianos/service"
)

var logger service.Logger

type program struct {
	exit    chan struct{}
	service service.Service
	cmd     *exec.Cmd
}

func (p *program) Start(s service.Service) error {
	go p.run()
	return nil
}

func (p *program) run() {
	// do work here
	p.cmd.Dir = "C:\\Users\\ewu\\go\\src\\github.com\\wys35\\GoAPIserver"
	p.cmd.Stdout = os.Stdout
	p.cmd.Stderr = os.Stderr
	err := p.cmd.Run()
	if err != nil {
		logger.Warningf("Error running: %v", err)
	}

	defer func() {
		if service.Interactive() {
			p.Stop(p.service)
		} else {
			p.service.Stop()
		}
	}()
}

func (p *program) Stop(s service.Service) error {
	// stop should not block.
	close(p.exit)
	// if p.cmd.ProcessState.Exited() == false {
	p.cmd.Process.Kill()
	// }
	if service.Interactive() {
		os.Exit(0)
	}
	return nil
}

func main() {
	svcConfig := &service.Config{
		Name:        "GoServiceExample",
		DisplayName: "Go Service Example",
		Description: "This is an exmaple Go Service.",
	}

	prg := &program{exit: make(chan struct{}),
		cmd: exec.Command("C:\\Users\\ewu\\go\\src\\github.com\\wys35\\GoAPIserver\\GoAPIServer.exe")}

	s, err := service.New(prg, svcConfig)
	prg.service = s
	if err != nil {
		log.Fatal(err)
	}
	logger, err = s.Logger(nil)
	if err != nil {
		log.Fatal(err)
	}
	err = s.Run()
	if err != nil {
		log.Fatal(err)
	}
}
