package isogram

import (
	"fmt"
	"strings"
)

func IsIsogram(word string) bool {
	m := make(map[string]int)
	chars := []rune(strings.ToLower(word))
	for i := 0; i < len(chars); i++ {
		char := string(chars[i])
		if char == "-" || char == " " {

		} else {
			m[char]++
		}
		if m[char] == 2 {
			return false
		}
	}
	return true
}

func main(word string) {
	fmt.Println(IsIsogram(word))
}
